import { singleton } from 'tsyringe';
import { HashService } from 'shared/services/hash/hash.service';
const db = require('database/models');
const { Sequelize, sequelize, User } = db.default;
const { Op } = Sequelize;

@singleton()
export class AuthService {
  constructor(private readonly hashService: HashService) {}
}

import { container } from 'tsyringe';
import environment from 'configs/environment.constants';
import { IModule } from '../../../app.module';
import { NOTIFICATION_OPTIONS } from './constants';
import { EmailChannel } from 'shared/services/notifications/channels/email/email.channel';
import { WebSocketChannel } from './channels/websocket/websocket.channel';
import { OneSignalChannel } from './channels/onesignal/onesignal.channel';
import { SOCKET_SERVER } from '../../../app.constants';
import { SmsChannel } from './channels/sms-south-telecom/sms.channel';

export class NotificationModule implements IModule {
  registers = [
    container.register(NOTIFICATION_OPTIONS, {
      useValue: {
        channels: [
          // email channel
          EmailChannel.config({
            host: environment.mail_host,
            port: environment.mail_port as number,
            secure: Number(environment.mail_port) === 465,
            auth: {
              user: environment.mail_username,
              pass: environment.mail_password,
            },
          }),
          // websocket channel
          WebSocketChannel.config({
            socketServer: container.resolve(SOCKET_SERVER),
          }),
          // you can register any channel notification bellow
        ],
      },
    }),
  ];
  imports = [];
}

export const IS_ACTIVE = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const IS_LIVESTREAM = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const IS_GIFT_ACTIVE = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const IS_DEFAULT = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const CONFIG_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const CONFIG_TYPE = {
  ATTENDANCE: 1, // điểm danh
  ORDER_PROMOTION: 2, // đặt hàng
  DAILY_TURN: 3, // lượt quay hằng ngày
  REFERRAL_APP: 4, // giới thiệu app
  REFERRAL_CODE: 5, // giới thiệu mã
  REGISTER: 6, // đăng kí tài khoản thành công
};

export const ONESIGNAL = {
  APP_ID: 'e0d9b6b9-a033-4737-8fc2-1ee526338945',
  AUTHORIZATION: 'YmMxODRiZWMtYmM4ZS00NTZmLWIwZmUtMzE1YTNmNjQyZmM5',
  ANDROID_CHANNEL_ID: 'b2c24e1a-393d-41c5-a6ea-ee6826e8e9b0',
};

export const GIFT_STATUS = {
  PENDING: 0, // đang đợi xác nhận gửi quà
  CONFIRMED: 1, // đã xác nhận, có thể sử dụng
  SENT_GIFT: 2, // đã gửi quà cho người nhận
  USED: 3, // trạng thái quà đã sử dụng
};
export const GIFT_OWNER_STATUS = {
  USED: 1,
  NOT_USED: 2,
};

export const GIFT_TYPE = {
  GIFT: 1,
  DISCOUNT_CODE: 2,
};

export const FILTER_TYPE = {
  PRICE_MIN_MAX: 2, // Giá từ thấp đến cao
  PRICE_MAX_MIN: 1, // Giá từ cao đến thấp
  TIME_NEW_OLD: 1, // Tin đăng mới nhất
  TIME_OLE_NEW: 2, // Tin đăng cũ nhất
};
export const BUY_TYPE = {
  ALL: 0,
  SELL: 1,
  NOTSELL: 2,
};
export const IS_PUSH = {
  PUSHED: 1,
  UN_PUSHED: 0,
};

export const DF_NOTIFICATION = {
  ALL: 1, // thông báo tất cả
  ORDER_SHOP: 2, // thông báo trạng thái đơn hàng
  COMMENT_POST: 3, // thông báo có người bình luận bài viết
  LIKE_POST: 4, // thông báo có người thích bài viết
  SEND_COMMENT: 5, // thông báo shop trả lời bình luận
  LIKE_COMMENT: 6, // thông báo shop thích bình luận
  SHOP_CREATE_LIVESTREAM: 7, // thông báo shop tạo livestream
  REGISTER_USER: 8, // thông báo đăng kí tài khoản thành công được cộng điểm
  PURCHASE_GIFT: 9, // thông báo có yêu cầu đổi quà web admin
  CONFIRM_PURCHASE_GIFT: 10, // Thông báo trạng thái đổi quà của bạn
  NEW_ORDER: 11, // Thông báo shop có đơn hàng cần duyệt
  GIFT_EXCHANGE: 12, // Thông báo trừ điểm
  NEWS_POST: 13, // Thông báo bài viết
  REFERRAL_APP: 14, // Giới thiệu APP thành công
  PROMOTION_POINT: 15, // Cộng điểm khi đặt hàng thành công
  REFERRAL_CODE: 16, // Cộng điểm khi nhập mã thành công
  NEW_MESSAGE: 17, // Có tin nhắn mới
  ORDER_CANCEL: 18, // kHÁCH HÀNG HỦY ĐƠN HÀNG
};

export const PANCAKE = 'https://pos.pages.fm/api/v1';

export const STATUS_REFERAL = {
  PENDING: 0, // Chưa nhập mã gt
  SUCCESS: 1, // Đã nhập mã gt
  FINISHED: 2, // Đã cộng điểm cho người hoàn thành đơn đầu tiên
};

export const SOCKET = {
  ADMIN: 'admin',
  SHOP: 'shop',
};

export const STOKE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const ORDER_STATUS = {
  PENDING: 1, // chờ xác nhận
  CONFIRMED: 2, // đã xác nhận
  SHIP: 3, // đang giao
  SUCCCESS: 4, // hoàn thành
  CANCELED: 5, // huỷ
};

export const REPORT_TYPE = {
  CATEGORY: 1, // Nhóm sản phẩm
  CUSTOMER: 2, // Theo khách hàng
};

export const PRODUCT_TYPE = {
  DETAIL: 1, // Thông tin sản phẩm
  SELL: 2, // Khách hàng mua sp
  ORDER: 3, // Danh sách đơn hàng
};

export const TYPE_POINT_TRANSACTION_HISTORY = {
  ADD: 1,
  SUB: 0,
};
export const TYPE_TRANSACTION_POINT = {
  REGISTER_SUCCESS: 1, // đăng kí thành công
  REFERRAL_CODE: 2, // nhập mã giới thiệu thành công
  REFERRAL_APP: 3, // giới thiệu ứng dụng thành công
  ORDER_PROMOTION: 4, // đặt hàng thành công
  PURCHASE_GIFT: 5, // Đổi quà thành công
  PURCHASE_DISCOUNT: 6, // Đổi mã giảm giá thành công
};
export const PRODUCT_STATUS = {
  AVAILABLE: 1, // Hoạt động
  UNAVAILABLE: 0, // ngưng hđ
};
export const PRODUCT_STOCK_STATUS = {
  AVAILABLE: 1, // còn hàng
  UNAVAILABLE: 0, // hết hàng
};
export const PRODUCT_PRICE_STATUS = {
  AVAILABLE: 1,
  UNAVAILABLE: 0,
};
export const MEDIA_TYPE = {
  IMAGE: 0,
  VIDEO: 1,
};
export const GENDER = {
  MALE: 1,
  FEMALE: 2,
};
export const ROLE = {
  ADMIN: 1,
  SHOP: 2,
  CUSTOMER: 3,
  ADMIN_EDITOR: 4,
  ADMIN_SELL_MANAGER: 5,
  ADMIN_SERVICE_MANAGER: 6,
  SHOP_MEMBER: 7,
};

export const ROLE_NAMES = {
  ADMIN: 'admin',
  ADMIN_EDITOR: 'admin_editor',
  ADMIN_SELL_MANAGER: 'admin_sell_manager',
  ADMIN_SERVICE_MANAGER: 'admin_service_manager',
  SHOP: 'shop',
  SHOP_MEMBER: 'shop_member',
  CUSTOMER: 'customer',
};
export const SHOP_ROLE = {
  SHOP: 2,
  SHOP_MEMBER: 7,
};
export const ADMIN_ROLE = {
  ADMIN: 1,
  ADMIN_EDITOR: 4,
  ADMIN_SELL_MANAGER: 5,
  ADMIN_SERVICE_MANAGER: 6,
};

export const CUSTOMER = {
  GENERAL: 1,
  ORDER: 2,
  HISTORY: 3,
};
export const CART_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const USER_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const CATEGORY_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const TOPIC_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const TOPIC_TYPE = {
  ADMIN: 0,
  NORMAL: 1,
};
export const DAILY_ATTENDANCE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const IS_READ = {
  READ: 1,
  NOT_READ: 0,
};
export const RATE = {
  REVIEW_EXIT: 'Đơn hàng đã được đánh giá',
};
export const USER_TOPIC_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};

export const TAB = {
  PRODUCT: 0,
  CATEGORY: 1,
  LIVESTREAM: 2,
  POST: 3,
};

export const TAB_SHOP = {
  HISTORY: 0,
  PRODUCT: 1,
  ORDER: 2,
  LIVESTREAM: 3,
};

export const USER_TOPIC_MESSAGE_STATUS = {
  UNBLOCK: 1,
  BLOCK: 0,
};
export const LIVESTREAM_ENABLE = {
  ACTIVE: 1,
  INACTIVE: 0,
};

export const IS_HIGHLIGHT_LIVESTREAM_PRODUCT = {
  HIGHLIGHT: 1,
  NOT_HIGHLIGHT: 0,
};

export const REACTION_TYPE = {
  LIKE: 1,
  HEART: 2,
  LOVE: 3,
  HAHA: 4,
  SURPRISE: 5,
  SAD: 6,
  ANGRY: 7,
};
export const LIVESTREAM_STATUS = {
  INITIAL: 0,
  STREAMING: 1,
  FINISHED: 2,
};
export const SHOP_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};
export const POST_STATUS = {
  ALL: 1, // thông báo cho tất cả
  CUSTOMER: 2, // thông báo chỉ cho khách hàng
  SHOP: 3, // thông báo chỉ tài khoản gian hàng
};
export const IS_POSTED = {
  POSTED: 1, // đã đăng
  NOT_POSTED: 0, // chưa đăng
};

export const PAKAGE_CATEGORY = {
  ADD_PAKAGE: 1,
  SUB_PAKAGE: 2,
};

export const PAKAGE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};

export const YOUTUBE_WORKER_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
};

export const CONFIG = {
  DATE_FORMAT: 'YYYY-DD-MM-DD',
  CRYPT_SALT: 10,
  PAGING_LIMIT: 24,
  RESET_PASSWORD: 'Base123a@',
  MAX_IMAGE: 5,
  OTP_FAIL_COUNT: 3,
  EXP_OTP: 60,
  EXP_PASSWORD: 60,
  HOT_LINE: '0394202944',
  LOGIN_FAIL_COUNT: 4,
  SHOP_REPLY_MESSAGE:
    'Xin chào !\n Cảm ơn quý khách đã quan tâm đến sản phẩm của shop. Chúng tôi sẽ trả lời quý khách trong thời gian sớm nhất.',
};

export const GOOGLE_API_KEY = 'AIzaSyAWWFzb1uMxLnm_DaD0ZVfZXrBzeEfrJTM';
export const PLACE_TYPES = {
  PROVINCE: 'region',
  DISTRICT: 'locality',
  WARD: 'extended-address',
};

let SERVER_URL = '';
export function updateServerUrl(request): string {
  SERVER_URL = `${request.protocol}://${request.headers.host}`;
  return SERVER_URL;
}

export function getServerUrl(): string {
  return SERVER_URL;
}

export function getFullUrl(path?: string): string {
  if (!path) {
    return null;
  }

  if (!path.startsWith('http')) {
    return `${SERVER_URL}/${path}`;
  }
  return path;
}

import { IS_ACTIVE, MEDIA_TYPE, getFullUrl } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const RotationLuckHistory = sequelize.define(
    'RotationLuckHistory',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: { allowNull: false, type: DataTypes.INTEGER },
      lucky_spin_gift_id: { allowNull: false, type: DataTypes.INTEGER },
      lucky_spin: { type: DataTypes.JSON },
      value: { allowNull: false, type: DataTypes.INTEGER },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'rotation_luck_history',
      version: true,
      hooks: {},
    },
  );

  RotationLuckHistory.associate = (db) => {
    // many - one
    db.RotationLuckHistory.belongsTo(db.User, {
      foreignKey: { name: 'user_id' },
    });
    db.RotationLuckHistory.belongsTo(db.ConfigLuckySpinGift, {
      foreignKey: { name: 'lucky_spin_gift_id' },
    });
  };

  return RotationLuckHistory;
};

import { IS_ACTIVE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const PakageHistory = sequelize.define(
    'PakageHistory',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      pakage_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      pakage_category_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      pakage_category_name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      current_minutes: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      minus: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      price: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'pakage_history',
      version: true,
      hooks: {},
    },
  );

  PakageHistory.associate = (db) => {
    // many - one
    db.PakageHistory.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.PakageHistory.belongsTo(db.Pakage, {
      foreignKey: { name: 'pakage_id' },
    });
  };

  return PakageHistory;
};

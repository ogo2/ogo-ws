import { Server as SocketServer, Socket } from 'socket.io';
import { Server as HttpServer } from 'http';
import { socketAuthentication } from 'middlewares/auth-io.middleware';
import { initDisconnectEvents, initGateWays } from './gateways';
import { logger } from 'shared/logger/logger';
import { RedisAdapter } from './adapters/redis/redis.adapter';
import { IIOAdapter } from './interfaces/IAdapter';
export class WebSocket extends SocketServer {
  constructor(server: HttpServer, option: { adapter: IIOAdapter }) {
    const { adapter } = option;
    super(server, {
      cors: {
        origin: ['http://localhost:3000', 'http://ogo.winds.vn', 'https://admin.ogogroup.info'],
        allowedHeaders: ['token'],
        credentials: true,
        methods: ['GET', 'POST'],
        preflightContinue: false,
        optionsSuccessStatus: 200,
      },
      pingInterval: 60 * 1000, // 5s - đơn vị mili seconds 1000ms = 1s
      pingTimeout: 60 * 1000, // 20s - đơn vị mili seconds 1000ms = 1s
      cookie: false,
    });
    this.adapter(adapter.createAdapter());
    this.use(socketAuthentication);
    this.bindClientConnect();
  }
  public static createWebSocket(server: HttpServer) {
    const option = {
      adapter: new RedisAdapter(),
    };
    return new WebSocket(server, option);
  }

  public bindClientConnect(): void {
    this.on('connection', async (socket: Socket) => {
      const { user } = socket.handshake.auth;
      initGateWays(user, socket);
      socket.on('disconnect', (reason) => {
        logger.warn({ message: `Socket disconnect with ${reason}` });
        initDisconnectEvents(user, socket);
      });
    });
  }
}

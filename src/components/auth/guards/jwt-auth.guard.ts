import { singleton } from 'tsyringe';
import jwt from 'jsonwebtoken';
import environment from 'configs/environment.constants';
import { Authorization } from '../types/Authorization';
import { JWTPayload } from '../types/JWTPayload';

@singleton()
export class JWTAuthGuard {
  sign(payload: JWTPayload) {
    return jwt.sign({ ...payload }, environment.app_secret, {
      expiresIn: environment.jwt_tll,
      algorithm: 'HS256',
    });
  }

  verify(token: string): Promise<Authorization> {
    return new Promise((resolve, reject) => {
      jwt.verify(token, environment.app_secret, (err: Error, user: Authorization) => {
        if (err || !user) {
          return reject(err);
        }
        resolve(user);
      });
    });
  }

  signResetPasswordToken(payload: Pick<JWTPayload, 'id'>) {
    return jwt.sign({ ...payload }, environment.reset_password_secret, {
      expiresIn: environment.jwt_tll,
      algorithm: 'HS256',
    });
  }

  verifyResetPasswordToken(token: string): Promise<Omit<Authorization, 'petrol_store_id' | 'roles'>> {
    console.log('token very', token);
    return new Promise((resolve, reject) => {
      jwt.verify(
        token,
        environment.reset_password_secret,
        (err: Error, user: Omit<Authorization, 'petrol_store_id' | 'roles'>) => {
          if (err || !user) {
            return reject(err);
          }
          resolve(user);
        },
      );
    });
  }
}

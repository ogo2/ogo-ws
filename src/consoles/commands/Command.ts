import { ICommand } from '../interface/ICommand';
import { IOption } from '../interface/IOption';

export abstract class Command implements ICommand {
  public abstract signature(): string;
  public abstract description(): string;
  public abstract options(): IOption[];
}

export const Info = (content: string): void => {
  console.log('\x1b[32m%s\x1b[0m', content);
};

export const Warning = (content: string): void => {
  console.log('\x1b[33m%s\x1b[0m', content);
};

export const Error = (content: string): void => {
  console.log('\x1b[31m%s\x1b[0m', content);
  process.exit(1);
};

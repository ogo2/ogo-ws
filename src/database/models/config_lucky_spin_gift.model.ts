import { IS_ACTIVE, CONFIG_STATUS } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const ConfigLuckySpinGift = sequelize.define(
    'ConfigLuckySpinGift',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      value: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      percent: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      max_per_day: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(CONFIG_STATUS),
        defaultValue: CONFIG_STATUS.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(CONFIG_STATUS)],
            msg: 'Invalid config status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'config_lucky_spin_gift',
      version: true,
      hooks: {},
    },
  );

  ConfigLuckySpinGift.associate = (db) => {
    // one - many
    db.ConfigLuckySpinGift.hasMany(db.RotationLuckHistory, {
      foreignKey: { name: 'lucky_spin_gift_id' },
    });
  };

  return ConfigLuckySpinGift;
};

import 'reflect-metadata';
import { container } from 'tsyringe';
import { JWTAuthGuard } from './jwt-auth.guard';

describe('AuthService', () => {
  let service: JWTAuthGuard;
  beforeEach(async () => {
    container.clearInstances();
    service = container.resolve(JWTAuthGuard);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

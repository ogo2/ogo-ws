import { default as slugify } from 'slugify';
import _ from 'lodash';
import moment from 'moment';
import environment from 'configs/environment.constants';
class Utils {
  /**
   * Docstring Utils helper function
   */
  static sortObject(obj: any) {
    var sorted = {};
    var str = [];
    var key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) {
        str.push(encodeURIComponent(key));
      }
    }
    str.sort();
    for (key = 0; key < str.length; key++) {
      sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, '+');
    }
    return sorted;
  }
  static cleanupText(text: string): string {
    return (
      text &&
      text
        .trim()
        .replace(/\n{2,}/g, '\n\n')
        .replace(/ +/g, ' ')
    );
  }
  static trimStringProperties(obj) {
    if (obj !== null && typeof obj === 'object') {
      for (let prop in obj) {
        if (typeof obj[prop] === 'object') {
          return Utils.trimStringProperties(obj[prop]);
        }
        if (typeof obj[prop] === 'string') {
          obj[prop] = Utils.cleanupText(obj[prop]);
        }
      }
    }
  }
  static generateSlug(name: string): string {
    const slug: string = slugify(name, {
      replacement: '-',
      remove: undefined,
      lower: true,
    });
    const s = `${slug}-${moment().unix()}`;
    return s;
  }
  static cleanEmptyProperties(object: Object) {
    return _.omitBy(object, (v: any) => _.isUndefined(v) || _.isNull(v) || v === '');
  }

  static formatDateString(time: Date | string | number) {
    return moment(new Date(time)).format('HH:mm DD/MM/YYYY');
  }

  static formatPhoneNumber(phone: string) {
    let validPhone = phone;
    if (phone.startsWith('84')) {
      return '0' + validPhone.slice(2);
    } else if (phone.startsWith('+84')) {
      return '0' + validPhone.slice(3);
    }
    return validPhone;
  }
  static injectObject2String(str: string, obj: Object = {}) {
    const keys = Object.keys(obj);
    keys.forEach((key: string) => {
      str = str.replace(`{{${key}}}`, obj[key]);
    });
    return str;
  }
  public static timer(ms: number) {
    return new Promise((res) => setTimeout(res, ms));
  }
  public static getFullUrl(path?: string): string {
    if (!path) {
      return null;
    }
    if (!path.startsWith('http')) {
      const correctPath = path.replace(/^\//g, '');
      return `${environment.s3_aws_url}/${correctPath}`;
    }
    return path;
  }
  public static getBaseMediaUrl(): string {
    return environment.s3_aws_url;
  }
}
export { Utils };

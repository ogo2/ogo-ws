export type ResponseOption = {
  message?: string;
  code?: number;
  status?: boolean;
};

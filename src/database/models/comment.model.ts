import { IS_ACTIVE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Comment = sequelize.define(
    'Comment',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      post_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      livestream_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      parent_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      target_user_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      content: { type: DataTypes.TEXT },
      count_like: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      count_comment: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'comment',
      version: true,
      hooks: {},
    },
  );

  Comment.associate = (db) => {
    // one - many
    db.Comment.hasMany(db.Reaction, { foreignKey: { name: 'comment_id' } });
    db.Comment.hasMany(db.Comment, { foreignKey: { name: 'parent_id' } });
    // many - one
    db.Comment.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Comment.belongsTo(db.Post, { foreignKey: { name: 'post_id' } });
    db.Comment.belongsTo(db.Livestream, {
      foreignKey: { name: 'livestream_id' },
    });
    db.Comment.belongsTo(db.Comment, { foreignKey: { name: 'parent_id' } });
    db.Comment.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.Comment.belongsTo(db.User, { as: 'target_user', foreignKey: 'target_user_id' });
  };

  return Comment;
};

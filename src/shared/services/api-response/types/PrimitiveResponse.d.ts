export type PrimitiveResponse = {
  data: { [key: string]: any };
  code?: number;
  status?: boolean;
  message?: string;
};

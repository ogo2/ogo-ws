export type DataResponse<T> = {
  data: T;
  code?: number;
  status?: boolean;
  message?: string;
};

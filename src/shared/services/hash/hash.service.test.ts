import 'reflect-metadata';
import { container } from 'tsyringe';
import { HashService } from './hash.service';

describe('HashService', () => {
  let service: HashService;
  beforeEach(async () => {
    container.clearInstances();
    service = container.resolve(HashService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

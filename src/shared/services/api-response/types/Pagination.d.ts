export type Pagination = {
  page: number;
  totalItems: number;
  limit: number;
};

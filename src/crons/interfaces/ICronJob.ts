import { CronJob } from 'cron';
import { ICronTime } from 'crons/cron.constant';

export interface ICronJob {
  cronTime: ICronTime;
  execute(): Promise<CronJob> | CronJob;
}

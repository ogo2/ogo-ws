export const getMockSeeder = () => `import { QueryInterface, Sequelize } from 'sequelize/types';
    
export default {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
`;

require('dotenv').config();

export const development = {
  database: process.env.MYSQL_DATABASE,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  host: process.env.MYSQL_HOST,
  port: parseInt(process.env.MYSQL_PORT),
  dialect: process.env.DB_TYPE,
  logging: false,
};

export const test = {
  database: process.env.MYSQL_DATABASE,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  host: process.env.MYSQL_HOST,
  port: parseInt(process.env.MYSQL_PORT),
  dialect: process.env.DB_TYPE,
  logging: false,
};

export const production = {
  database: process.env.MYSQL_DATABASE,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  host: process.env.MYSQL_HOST,
  port: parseInt(process.env.MYSQL_PORT),
  dialect: process.env.DB_TYPE,
  logging: false,
};

export default {
  // project config
  app_name: process.env.APP_NAME,
  app_env: process.env.APP_ENV,
  app_secret: process.env.SECRET,
  jwt_tll: process.env.JWT_TTL,
  reset_password_secret: process.env.RESET_PASSWORD_SECRET,
  reset_password_ttl: process.env.RESET_PASSWORD_TTL,
  port: process.env.WS_PORT ? parseInt(process.env.WS_PORT) : 3000,
  // db config
  db_type: process.env.DB_TYPE,
  db_host: process.env.MYSQL_HOST,
  db_port: parseInt(process.env.MYSQL_PORT),
  db_username: process.env.MYSQL_USER,
  db_password: process.env.MYSQL_PASSWORD,
  db_database: process.env.MYSQL_DATABASE,
  // mail config
  mail_mailer: process.env.MAIL_MAILER,
  mail_host: process.env.MAIL_HOST,
  mail_port: process.env.MAIL_PORT || parseInt(process.env.MAIL_PORT) || 587,
  mail_username: process.env.MAIL_USERNAME,
  mail_password: process.env.MAIL_PASSWORD,
  mail_encryption: process.env.MAIL_ENCRYPTION,
  mail_from_address: process.env.MAIL_FROM_ADDRESS,
  mail_from_name: process.env.MAIL_FROM_NAME,
  default_sender: process.env.DEFAULT_SENDER,
  default_sender_email: process.env.DEFAULT_SENDER_EMAIL,
  // s3 config
  s3_access_key: process.env.S3ACCESS_KEY,
  s3_secret_key: process.env.S3SECRET_KEY,
  s3_region: process.env.S3REGION,
  s3_bucket: process.env.S3BUCKET,
  s3_aws_url: process.env.S3URL,
  // onesignal config
  onesignal_app_id: process.env.ONESIGNAL_APP_ID,
  onesignal_authorization: process.env.ONESIGNAL_AUTHORIZATION,
  onesignal_android_channel_id: process.env.ONESIGNAL_ANDROID_CHANNEL_ID,
  // sms config
  sms_authorization: process.env.SMS_AUTHORIZATION,
  sms_url_version: process.env.SMS_API_URL_VERSION,
  // redis
  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD,
  },
};

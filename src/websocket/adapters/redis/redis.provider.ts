import { createClient, RedisClient } from 'redis';
import environment from 'configs/environment.constants';
export const redisProvider: RedisClient = createClient({ ...environment.redis });
export const getRefRedisConnection = () => redisProvider.duplicate();

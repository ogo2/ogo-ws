import { singleton } from 'tsyringe';
import { IChannel } from '../../interfaces/channel.interface';
import { SMS } from './constants';
import { INotification } from '../../interfaces';
import axios, { AxiosInstance } from 'axios';
import { logger } from 'shared/logger/logger';

export type OptionSms = {
  sms_authorization: string;
  sms_url_version: string;
};

@singleton()
export class SmsChannel implements IChannel {
  public name: string;
  private apiInstance: AxiosInstance;

  constructor(options: { name: string; apiInstance: AxiosInstance }) {
    const { name, apiInstance } = options;
    this.name = name;
    this.apiInstance = apiInstance;
  }

  public static config(options: OptionSms) {
    const apiInstance = axios.create({
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Basic ${options.sms_authorization}`,
      },
      timeout: 20000,
      responseType: 'json',
      baseURL: options.sms_url_version,
    });

    return new SmsChannel({ name: SMS, apiInstance });
  }

  async execute(notification: INotification): Promise<any> {
    const smsable = (notification as any).toPhone(notification.notifiable);
    try {
      const result = await this.apiInstance.post('/webapi/sendSMS', smsable.getParams());
      return result;
    } catch (error) {
      logger.error({ message: error.response.data });
    }
  }
}

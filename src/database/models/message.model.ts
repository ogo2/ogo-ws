import { IS_ACTIVE, IS_READ, getFullUrl } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Message = sequelize.define(
    'Message',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      topic_message_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      reply_message_id: {
        type: DataTypes.INTEGER,
      },
      content: {
        allowNull: true,
        type: DataTypes.TEXT,
      },
      message_media_url: {
        allowNull: true,
        type: DataTypes.STRING,
        get() {
          return getFullUrl(this.getDataValue('message_media_url'));
        },
      },
      type_message_media: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      is_read: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: IS_READ.NOT_READ,
        values: Object.values(IS_READ),
        validate: {
          isIn: {
            args: [Object.values(IS_READ)],
            msg: 'Invalid is read message.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'message',
      version: true,
      hooks: {},
    },
  );

  Message.associate = (db) => {
    // one - many
    db.Message.hasMany(db.Message, {
      foreignKey: { name: 'reply_message_id' },
    });

    // many - one
    db.Message.belongsTo(db.Message, {
      foreignKey: { name: 'reply_message_id' },
    });
    db.Message.belongsTo(db.TopicMessage, {
      foreignKey: { name: 'topic_message_id' },
    });
    db.Message.belongsTo(db.User, {
      foreignKey: { name: 'user_id' },
    });
    db.Message.belongsTo(db.Shop, {
      foreignKey: { name: 'shop_id' },
    });
  };

  return Message;
};

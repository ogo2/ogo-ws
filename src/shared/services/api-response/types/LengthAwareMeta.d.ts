import { Pagination } from './Pagination';

export type LengthAwareMeta = {
  pagination?: Pagination;
};

export const getMockModel = (
  modelName: string,
  model: string,
) => `import { IS_ACTIVE } from 'database/database.constants';
import { DataTypes } from 'sequelize';
module.exports = function (sequelize, _) {
  const ${modelName} = sequelize.define(
    '${modelName}',
    {
      // PK
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },

      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER({ length: 2 }),
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid active status.',
          },
        },
      },
      create_by: { allowNull: true, type: DataTypes.INTEGER },
      update_by: { allowNull: true, type: DataTypes.INTEGER },
      delete_by: { allowNull: true, type: DataTypes.INTEGER },
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      tableName: '${model.split('-').join('_')}',
      createdAt: false,
      updatedAt: false,
      deletedAt: false,
      paranoid: false,
      timestamps: true,
      freezeTableName: true,
      version: true,
      hooks: {},
    },
  );
  /**
   * hooks
   */
   ${modelName}.beforeUpdate((instance, options) => {
    instance.changed('is_active') ? (instance.delete_at = new Date()) : (instance.update_at = new Date());
  });
  ${modelName}.associate = (db) => {
    // many - one
    // db.${modelName}.belongsTo(db.ForeignTB, { as: 'foreign_tb', foreignKey: 'foreign_key_id' });
    // one - many
    // db.${modelName}.hasMany(db.ForeignTB, { as: 'foreign_tb', foreignKey: { name: 'foreign_key_id' } });
  };
  return ${modelName};
};
    
    `;

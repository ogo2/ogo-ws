import { IS_ACTIVE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const TopicMessage = sequelize.define(
    'TopicMessage',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      shop_id: { allowNull: false, type: DataTypes.INTEGER },
      user_id: { allowNull: false, type: DataTypes.INTEGER },
      // last_message: {
      //   type: DataTypes.STRING,
      // },
      time_last_send: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'topic_message',
      version: true,
      hooks: {},
    },
  );

  TopicMessage.associate = (db) => {
    // one - many
    db.TopicMessage.hasMany(db.Message, {
      foreignKey: { name: 'topic_message_id' },
    });

    // many - one
    db.TopicMessage.belongsTo(db.User, {
      foreignKey: { name: 'user_id' },
    });
    db.TopicMessage.belongsTo(db.Shop, {
      foreignKey: { name: 'shop_id' },
    });
  };

  return TopicMessage;
};

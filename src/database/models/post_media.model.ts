import { IS_ACTIVE, MEDIA_TYPE, getFullUrl } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const PostMedia = sequelize.define(
    'PostMedia',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      post_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      media_url: {
        allowNull: false,
        type: DataTypes.STRING,
        get() {
          return getFullUrl(this.getDataValue('media_url'));
        },
      },
      type: {
        allowNull: true,
        type: DataTypes.INTEGER,
        values: Object.values(MEDIA_TYPE),
        validate: {
          isIn: {
            args: [Object.values(MEDIA_TYPE)],
            msg: 'Invalid type media.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'post_media',
      version: true,
      hooks: {},
    },
  );

  PostMedia.associate = (db) => {
    // many - one
    db.PostMedia.belongsTo(db.Post, { foreignKey: { name: 'post_id' } });
  };

  return PostMedia;
};

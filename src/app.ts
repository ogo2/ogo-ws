import express, { Express } from 'express';
import { json, urlencoded } from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import * as swaggerUi from 'swagger-ui-express';
import { API_DOCS } from 'configs/configs.constants';

export function createApp(): Express {
  const app: Express = express();

  app.use(cors());
  app.use(cors({ optionsSuccessStatus: 200 }));
  app.use(urlencoded({ extended: true }));
  app.use(json());
  app.use(morgan('combined'));
  app.use(express.static('public'));
  return app;
}

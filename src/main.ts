import 'reflect-metadata';
import './register-module-alias';
import './app.module';
import { container } from 'tsyringe';
import { HTTP_SERVER } from './app.constants';
import { HttpServer } from './http-server';
import { TaskRegister } from 'crons/TaskRegister';
import environment from './configs/environment.constants';
import { logger } from './shared/logger/logger';
import { sync } from 'configs/sequelize.config';

async function bootstrap() {
  console.log('environment', environment);
  const PORT = environment.port;
  const httpServer: HttpServer = container.resolve(HTTP_SERVER);
  httpServer.listen(PORT, () => logger.info({ message: `Server successfully started at: ${PORT}` }));

  TaskRegister.enforce();
  // ### sync database ### \\
  // sync();
}
bootstrap();

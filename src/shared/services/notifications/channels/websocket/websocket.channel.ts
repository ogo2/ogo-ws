import { singleton, container } from 'tsyringe';
import { Server as SocketServer } from 'socket.io';
import { IChannel } from '../../interfaces/channel.interface';
import { WEB_SOCKET } from './constants';
import { INotification } from '../../interfaces';
import e from 'cors';
import { ApiResponseService } from 'shared/services/api-response/api-response.service';

export type OptionWebSocketChannel = {
  socketServer: SocketServer;
};

@singleton()
export class WebSocketChannel implements IChannel {
  public name: string;
  private socketServer: SocketServer;
  private responseService: ApiResponseService;

  constructor(options: { name: string; socketServer: SocketServer; responseService: ApiResponseService }) {
    const { name, socketServer, responseService } = options;
    this.name = name;
    this.socketServer = socketServer;
    this.responseService = responseService;
  }

  public static config(options: OptionWebSocketChannel) {
    const responseService = container.resolve(ApiResponseService);
    return new WebSocketChannel({ name: WEB_SOCKET, socketServer: options.socketServer, responseService });
  }

  async execute(notification: INotification): Promise<any> {
    const websocketable = (notification as any).toChannel(notification.notifiable);
    const params = websocketable.getParams();
    if (params.to.length > 0) {
      params.to.forEach((room) => this.socketServer.to(room));
    }
    this.socketServer.emit(params.event, this.responseService.withData(params.data));
  }
}

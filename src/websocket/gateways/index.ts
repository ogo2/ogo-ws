import { Authorization } from 'components/auth/types';
import { Socket } from 'socket.io';
import {
  SOCKET_ON_LIVESTREAM_EVENT,
  SOCKET_ON_MESSAGE_CHANNEL_EVENT,
  withLiveStreamChannel,
  withTopicMessageChannel,
} from './on-event-gateway.constants';
export const initGateWays = (user: Authorization, io: Socket) => {
  // subscribe livestream
  io.on(SOCKET_ON_LIVESTREAM_EVENT.SUBSCRIBE_LIVESTREAM_CHANNEL, (livestream_id: number) => {
    io.join(withLiveStreamChannel(livestream_id));
  });
  io.on(SOCKET_ON_LIVESTREAM_EVENT.UNSUBSCRIBE_LIVESTREAM_CHANNEL, (livestream_id: number) => {
    io.leave(withLiveStreamChannel(livestream_id));
  });

  // subscribe message channel
  io.on(SOCKET_ON_MESSAGE_CHANNEL_EVENT.SUBSCRIBE_MESSAGE_CHANNEL, (topic_message_id: number) => {
    io.join(withTopicMessageChannel(topic_message_id));
  });
  io.on(SOCKET_ON_MESSAGE_CHANNEL_EVENT.UNSUBSCRIBE_MESSAGE_CHANNEL, (topic_message_id: number) => {
    io.leave(withTopicMessageChannel(topic_message_id));
  });
};

export const initDisconnectEvents = (user: Authorization, io: Socket) => {};

import * as fs from 'fs';
import * as path from 'path';
import * as Sequelize from 'sequelize';
const basename = path.basename(module.filename);
import { sequelize } from 'configs/sequelize.config';

const db = {};
fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf('.') !== 0 &&
      file !== basename &&
      (file.slice(-9) === '.model.ts' || file.slice(-9) === '.model.js'),
  )
  .forEach(function (file) {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model['name']] = model;
  });
Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db['sequelize'] = sequelize;
db['Sequelize'] = Sequelize;
export default db;

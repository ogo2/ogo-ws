import { Command, Error, Info } from '../Command';
import fse from 'fs-extra';
import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import { IOption } from '../../interface/IOption';
import { getMockModel } from './mocks/model.mock';

const pathDir = '../../../database/models';

export class MakeModelCommand extends Command {
  signature(): string {
    return 'make:model <model>';
  }

  description(): string {
    return 'Create new model file';
  }

  options(): IOption[] {
    return [{ key: 'override?', description: 'Override existing file' }];
  }

  async handle(model: string, options: { override: undefined | string }): Promise<any> {
    if (_.isNil(model) || model === '') {
      Error('Model name is required');
    }
    const file = path.resolve(__dirname, pathDir, `${model}.model.ts`);
    if (fs.existsSync(file) && (options.override === undefined || options.override.toString() !== 'true')) {
      Error(`${model} already exist`);
    }

    const arrayName = model.split('-');
    const modelName =
      arrayName.length > 1
        ? arrayName.reduce((f, b) => f.charAt(0).toUpperCase() + f.slice(1) + b.charAt(0).toUpperCase() + b.slice(1))
        : arrayName[0].charAt(0).toUpperCase() + arrayName[0].slice(1);

    const content = getMockModel(modelName, model);
    fse.outputFileSync(file, content);
    Info(`${file} is created`);

    return file;
  }
}

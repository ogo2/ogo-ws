export const getMockService = (
  upperCaseControllerName: string,
  serviceName: string,
) => `import { singleton } from 'tsyringe';
import { BaseService } from 'shared/services/base.service';

const db = require('database/models');
const { Sequelize, ${upperCaseControllerName} } = db.default;
const { Op } = Sequelize;

@singleton()
export class ${serviceName} extends BaseService<typeof ${upperCaseControllerName}> {
  public model = ${upperCaseControllerName};
  constructor() {
    super();
  }
}

    `;

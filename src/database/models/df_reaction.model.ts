import { IS_ACTIVE, getFullUrl } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const DFReaction = sequelize.define(
    'DFReaction',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      url_icon: {
        allowNull: false,
        type: DataTypes.STRING,
        get() {
          return getFullUrl(this.getDataValue('url_icon'));
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'df_reaction',
      version: true,
      hooks: {},
    },
  );

  DFReaction.associate = (db) => {
    // one - many
    db.DFReaction.hasMany(db.Reaction, {
      foreignKey: { name: 'df_reaction_id' },
    });
  };

  return DFReaction;
};

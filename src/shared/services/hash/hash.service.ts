import { singleton } from 'tsyringe';
import bcrypt from 'bcrypt';
import crypto from 'crypto';
import qs from 'qs';
import { Utils } from 'shared/utils/Utils';
@singleton()
export class HashService {
  private readonly saltRounds = 10;

  /**
   *
   * @param password input primitive password
   * @returns hashed password
   */
  hash(password: string): string {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(this.saltRounds), null);
  }

  /**
   *
   * @param password dto password to compare
   * @param hash old string hash password to compare
   * @returns bool
   */
  check(password: string, hash: string): boolean {
    if (!password || !hash) return false;
    return bcrypt.compareSync(password, hash);
  }

  /**
   *
   * @param hash crypto with secret
   * @returns hash crypto
   */
  hashObjectWithSecret(object: Record<string, unknown>, secret: string): string {
    const signData = qs.stringify(object, { encode: false });
    const hmac = crypto.createHmac('sha512', secret);
    const signed = hmac.update(new Buffer(signData, 'utf-8')).digest('hex');
    return signed;
  }

  generateOtp(len = 6): string {
    let add = 1,
      max = 12 - add;
    if (len > max) {
      return this.generateOtp(max) + this.generateOtp(len - max);
    }
    max = Math.pow(10, len + add);
    let min = max / 10;
    let number = Math.floor(Math.random() * (max - min + 1)) + min;
    return ('' + number).substring(add);
  }
}

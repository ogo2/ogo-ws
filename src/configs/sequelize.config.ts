import { Sequelize } from 'sequelize';
import { Dialect } from 'sequelize';
import { logger } from 'shared/logger/logger';
import environment from './environment.constants';

const sequelize: Sequelize = new Sequelize({
  dialect: environment.db_type as Dialect,
  host: environment.db_host,
  port: environment.db_port,
  database: environment.db_database,
  username: environment.db_username,
  password: environment.db_password,
  // query: { raw: true },
  logging: false,
  timezone: '+07:00',
  pool: {
    max: 30,
    min: 0,
    acquire: 60000,
    idle: 5000,
  },
});
sequelize
  .authenticate()
  .then(() => {
    logger.info({ message: `Connection has been established successfully.` });
  })
  .catch((err) => {
    logger.error({
      message: `Unable to connect to the database: ${JSON.stringify(err)}`,
    });
  });
const sync = (): void => {
  sequelize
    .sync({ force: false, alter: true, logging: console.log })
    .then((res) => {
      logger.info({ message: `Sequelize sync result: ${res}` });
    })
    .catch((error: Error) => {
      logger.error({ message: `Sequelize sync error: ${error}` });
    });
};

export { sync, sequelize };

import { IS_ACTIVE, TOPIC_STATUS, getFullUrl, TOPIC_TYPE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Topic = sequelize.define(
    'Topic',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      order: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      description: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(TOPIC_STATUS),
        defaultValue: TOPIC_STATUS.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(TOPIC_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      icon_url: {
        allowNull: true,
        type: DataTypes.STRING,
        get() {
          return getFullUrl(this.getDataValue('icon_url'));
        },
      },
      type_topic: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(TOPIC_TYPE),
        defaultValue: TOPIC_TYPE.NORMAL,
        validate: {
          isIn: {
            args: [Object.values(TOPIC_TYPE)],
            msg: 'Invalid type topic.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid is active.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'topic',
      version: true,
      hooks: {},
    },
  );

  Topic.associate = (db) => {
    // one - many
    db.Topic.hasMany(db.UserInterestTopic, {
      foreignKey: { name: 'topic_id' },
    });
    db.Topic.hasMany(db.UserIgnoreTopic, {
      foreignKey: { name: 'topic_id' },
    });
    db.Topic.hasMany(db.Post, { foreignKey: { name: 'topic_id' } });
  };

  return Topic;
};

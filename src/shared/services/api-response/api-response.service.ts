import { singleton } from 'tsyringe';
import { HttpCode } from './constants/api-response.constant';
import { DataResponse, PaginateResponse, PrimitiveResponse, SuccessResponse } from './types';
import { Pagination } from './types/Pagination';
import { ResponseOption } from './types/ResponseOption';

@singleton()
export class ApiResponseService {
  public withSuccess(data?: { success: boolean }, options?: ResponseOption): SuccessResponse {
    return { data: { success: true }, status: true, code: HttpCode.Success, ...options };
  }

  public withData<T>(data: T, options?: ResponseOption): DataResponse<T> {
    return { data, status: true, code: HttpCode.Success, ...options };
  }

  public withListData<T>(data: T[], options?: ResponseOption): DataResponse<T[]> {
    return { data, status: true, code: HttpCode.Success, ...options };
  }

  public withPrimitive<T>(data: { [key: string]: any }, options?: ResponseOption): PrimitiveResponse {
    return { data, status: true, code: HttpCode.Success, ...options };
  }

  public withPaginate<T>(data: T[], pagination: Pagination, options?: ResponseOption): PaginateResponse<T> {
    return {
      data,
      meta: {
        pagination,
      },
      status: true,
      code: HttpCode.Success,
      ...options,
    };
  }
}

import _ from 'lodash';

export interface ISmsParams {
  from: string;
  to: string;
  text: string;
  unicode?: null | 0 | 1; // 0 = null: normal, 1: unicode
  dlr?: 0 | 1;
  smsid?: string;
  messageid?: string;
  encrypted?: 0 | 1;
  contentid?: 0 | 1 | 2;
  mustencryptviettel?: 0 | 1;
}
export interface ISmsable {
  fromBrand(brandname: string): ISmsable;
  toPhone(phone: string): ISmsable;
  setText(text: string): ISmsable;
  setUnicode(isUnicode: null | 0 | 1): ISmsable;
  setDlr(dlr: 0 | 1): ISmsable;
  setSmsId(smsid: string): ISmsable;
  setMessageId(messageid: string): ISmsable;
  setEncrypted(isEncrypted: 0 | 1): ISmsable;
  setContentId(contentid: 0 | 1 | 2): ISmsable;
  setMustEncryptViettel(mustencryptviettel: 0 | 1): ISmsable;
  getParams(): ISmsParams;
}

export class Smsable implements ISmsable {
  private _from: string;
  private _to: string;
  private _text: string;
  private _unicode?: null | 0 | 1;
  private _dlr?: 0 | 1;
  private _smsid?: string;
  private _messageid?: string;
  private _encrypted: 0 | 1;
  private _contentid: 0 | 1 | 2;
  private _mustencryptviettel: 0 | 1;

  public fromBrand(brandname: string): ISmsable {
    this._from = brandname;
    return this;
  }

  public toPhone(phone: string): ISmsable {
    this._to = phone;
    return this;
  }

  public setText(text: string): ISmsable {
    this._text = text;
    return this;
  }

  public setUnicode(isUnicode: null | 0 | 1): ISmsable {
    this._unicode = isUnicode;
    return this;
  }

  public setDlr(dlr: 0 | 1): ISmsable {
    this._dlr = dlr;
    return this;
  }

  public setSmsId(smsid: string): ISmsable {
    this._smsid = smsid;
    return this;
  }

  public setMessageId(messageid: string): ISmsable {
    this._messageid = messageid;
    return this;
  }

  public setEncrypted(isEncrypted: 0 | 1): ISmsable {
    this._encrypted = isEncrypted;
    return this;
  }

  public setContentId(contentid: 0 | 1 | 2): ISmsable {
    this._contentid = contentid;
    return this;
  }
  public setMustEncryptViettel(mustencryptviettel: 0 | 1): ISmsable {
    this._mustencryptviettel = mustencryptviettel;
    return this;
  }

  public getParams(): ISmsParams {
    return {
      from: this._from,
      to: this._to,
      text: this._text,
      unicode: this._unicode,
      dlr: this._dlr,
      smsid: this._smsid,
      messageid: this._messageid,
      encrypted: this._encrypted,
      contentid: this._contentid,
      mustencryptviettel: this._mustencryptviettel,
    };
  }
}

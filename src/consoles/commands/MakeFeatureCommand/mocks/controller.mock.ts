export const getMockController = (
  feature: string,
  upperCaseControllerName: string,
  controllerName: string,
  serviceName: string,
) => `import {
  Response,
  SuccessResponse,
  Body,
  Request,
  Get,
  Path,
  Post,
  Put,
  Query,
  Route,
  Delete,
  Tags,
  Security,
  NoSecurity,
} from 'tsoa';
import { injectable } from 'tsyringe';
import { handleSingleFile } from 'middlewares/upload.middleware';
import { detectedDeviceMiddleware } from 'middlewares/detected-device.middleware';
import { Express } from 'shared/types/Express';
import {
  ConflictErrorResponse,
  ForbiddenErrorResponse,
  NotFoundErrorResponse,
  UnAuthorizedErrorResponse,
} from 'shared/services/api-response/models/errors';
import { ApiResponseService } from 'shared/services/api-response/api-response.service';
import { BaseErrorResponse } from 'shared/services/api-response/models/BaseErrorResponse';
import { HttpCode } from 'shared/services/api-response/constants/api-response.constant';
import { IS_ACTIVE } from 'database/database.constants';
import { ${serviceName} } from 'components/${feature}/services/${feature}.service';

@injectable()
@Route('${feature.split('_').join('-')}')
@Tags('${upperCaseControllerName}s')
export class ${controllerName} {
  constructor(private readonly responseService: ApiResponseService, private readonly ${
    serviceName.charAt(0).toLowerCase() + serviceName.slice(1)
  }: ${serviceName}) {}
}
`;

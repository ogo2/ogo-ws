export type SuccessResponse = {
  data: { success: boolean };
  code?: number;
  status?: boolean;
  message?: string;
};

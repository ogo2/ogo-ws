import { IS_ACTIVE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Stock = sequelize.define(
    'Stock',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      pancake_stock_id: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      shop_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      address: {
        type: DataTypes.STRING,
      },
      df_provine_id: {
        type: DataTypes.INTEGER,
      },
      df_district_id: {
        type: DataTypes.INTEGER,
      },
      df_ward_id: {
        type: DataTypes.INTEGER,
      },
      lat: { allowNull: true, type: DataTypes.INTEGER },
      long: { allowNull: true, type: DataTypes.INTEGER },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'stock',
      version: true,
      hooks: {},
    },
  );

  Stock.associate = (db) => {
    // one - many
    db.Stock.hasMany(db.ProductPrice, { foreignKey: { name: 'stock_id' } });
    db.Stock.hasMany(db.ProductStock, {
      foreignKey: {
        name: 'stock_id',
      },
    });
    db.Stock.hasMany(db.OrderItem, { foreignKey: { name: 'stock_id' } });
    // many - one
    db.Stock.belongsTo(db.DFProvince, {
      foreignKey: { name: 'df_provine_id' },
    });
    db.Stock.belongsTo(db.DFDistrict, {
      foreignKey: { name: 'df_district_id' },
    });
    db.Stock.belongsTo(db.DFWard, { foreignKey: { name: 'df_ward_id' } });
  };

  return Stock;
};

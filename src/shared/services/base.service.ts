import { Model, Sequelize, Transaction } from 'sequelize';
import { ApiResponseService } from './api-response/api-response.service';
import { ConflictErrorResponse, NotFoundErrorResponse } from './api-response/models/errors';
import environment from 'configs/environment.constants';
import { IS_ACTIVE } from 'database/database.constants';
import _ from 'lodash';

export interface IBaseService<E> {
  createOne: (data: E, options: Object) => Promise<E>;
  bulkCreate(data);

  findOne: (options: Object) => Promise<E>;
  findOneOrFail: (options: Object, error?: Error) => Promise<E>;
  findAll: (options: Object) => Promise<E[]>;
  findAndCountAll: (options: Object) => Promise<{ count: number; rows: Array<E[]> }>;

  update: (data: E, options: Object) => Promise<number[]>;
  updateAndFindOne: (data: E, options: Object) => Promise<E>;
  updateAndFindAll: (data: E, options: Object) => Promise<E[]>;

  updateOrFail: (data: E, options: Object, error?: Error) => Promise<number[]>;
  updateAndFindOneOrFail: (data: E, options: Object, error?: Error) => Promise<number[]>;

  inActive: (options: Object) => Promise<boolean>;
  inActiveOrFail: (options: Object, error?: Error) => Promise<boolean>;
  count: (options: Object) => Promise<number>;
  increment: (data: E, options: Object) => Promise<boolean>;

  destroy: (options: Object) => Promise<boolean>;
}

export class BaseService<E extends Model> implements IBaseService<E> {
  public model: any;
  constructor() {}

  async createOne(data: Object = {}, options: Object = {}) {
    return this.model.create(data, options).then((result) => {
      return result;
    });
  }

  async bulkCreate(data: Object = {}, options: Object = {}) {
    return this.model.bulkCreate(data, options).then((result) => {
      return result;
    });
  }

  async findOne(options: Object = {}) {
    return this.model.findOne(options);
  }
  async findOneOrFail(options: Object = {}, error?: Error) {
    const resource = await this.model.findOne(options);
    if (!resource)
      if (error) throw error;
      else throw new NotFoundErrorResponse(undefined, 'Resource not exist');
    return resource;
  }

  async findAll(options: Object = {}) {
    return this.model.findAll(options);
  }

  async findAndCountAll(options: Object = {}): Promise<{ count: number; rows: Array<any> }> {
    return this.model.findAndCountAll(options);
  }

  async update(data: any = null, options: Object = {}) {
    return this.model.update(data, options);
  }

  async updateAndFindOneOrFail(data: any = null, options: Object = {}, error?: Error) {
    const item = await this.findOne(options);
    if (!item)
      if (error) throw error;
      else throw new NotFoundErrorResponse(undefined, 'Resource not exist');
    await item.update(data, options);
    return item;
  }

  async updateAndFindOne(data: any = null, options: Object = {}) {
    const item = await this.findOne(options);
    await item.update(data);
    return item;
  }

  async updateAndFindAll(data: any = null, options: Object = {}) {
    await this.model.update(data, options);
    const list = await this.findAll(options);
    return list;
  }

  async updateOrFail(data: any = null, options: Object = {}, error?: Error) {
    const list = await this.findAll(options);
    if (list.length === 0)
      if (error) throw error;
      else throw new NotFoundErrorResponse(undefined, 'Resource not exist');
    return this.model.update(data, options);
  }

  async inActive(byUserId: number, options: Object = {}) {
    return this.model.update(
      {
        delete_by: byUserId,
        is_active: IS_ACTIVE.INACTIVE,
      },
      options,
    );
  }

  async inActiveOrFail(byUserId: number, options: Object = {}, error?: Error) {
    const list = await this.findAll(options);
    if (list.length === 0)
      if (error) throw error;
      else throw new NotFoundErrorResponse(undefined, 'Resource not exist');
    return this.model.update({ delete_by: byUserId, is_active: IS_ACTIVE.INACTIVE }, options);
  }
  async count(options: Object = {}) {
    const count = await this.model.count(options);
    return count;
  }
  async increment(data: Object = {}, options: Object = {}) {
    return this.model.increment(data, options);
  }

  async destroy(options: Object = {}) {
    return this.model.destroy(options);
  }
}

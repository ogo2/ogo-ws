import { IS_ACTIVE, USER_TOPIC_STATUS } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const UserIgnoreTopic = sequelize.define(
    'UserIgnoreTopic',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      topic_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: USER_TOPIC_STATUS.ACTIVE,
        values: Object.values(USER_TOPIC_STATUS),
        validate: {
          isIn: {
            args: [Object.values(USER_TOPIC_STATUS)],
            msg: 'Invalid user topic status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'user_ignore_topic',
      version: true,
      hooks: {},
    },
  );

  UserIgnoreTopic.associate = (db) => {
    // many - one
    db.UserIgnoreTopic.belongsTo(db.User, {
      foreignKey: { name: 'user_id' },
    });
    db.UserIgnoreTopic.belongsTo(db.Topic, {
      foreignKey: { name: 'topic_id' },
    });
  };

  return UserIgnoreTopic;
};

import { LIVESTREAM_ENABLE, SHOP_STATUS, IS_ACTIVE, getFullUrl } from 'database/database.constants';
import { Utils } from 'shared/utils/Utils';

module.exports = function (sequelize, DataTypes) {
  const Shop = sequelize.define(
    'Shop',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      // user_id: { type: DataTypes.INTEGER },
      name: { allowNull: false, type: DataTypes.STRING },
      phone: {
        // unique: true,
        allowNull: false,
        type: DataTypes.STRING,
      },
      pancake_shop_key: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      pancake_shop_id: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      stream_minutes_available: {
        allowNull: false,
        // defaultValue: 0,
        type: DataTypes.BIGINT,
      },

      google_tokens: {
        allowNull: true,
        type: DataTypes.JSON,
      },

      // cái này ko dùng nữa, xóa đi
      agora_project_id: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      agora_app_id: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      agora_app_certificate: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      //

      email: {
        // unique: true,
        allowNull: true,
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [6, 128],
            msg: 'Email address must be between 6 and 128 characters in length',
          },
          isEmail: {
            msg: 'Email address must be valid',
          },
        },
      },
      livestream_enable: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(LIVESTREAM_ENABLE),
        defaultValue: LIVESTREAM_ENABLE.INACTIVE,
        validate: {
          isIn: {
            args: [Object.values(LIVESTREAM_ENABLE)],
            msg: 'Invalid livestream enable status.',
          },
        },
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(SHOP_STATUS),
        defaultValue: SHOP_STATUS.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(SHOP_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      profile_picture_url: {
        type: DataTypes.STRING,
        get() {
          return Utils.getFullUrl(this.getDataValue('profile_picture_url'));
        },
      },

      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'shop',
      version: true,
      hooks: {},
    },
  );

  Shop.associate = (db) => {
    // one - many
    db.Shop.hasMany(db.User, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Livestream, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.PakageHistory, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Stock, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Product, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Order, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.TopicMessage, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Message, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Post, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Comment, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Reaction, { foreignKey: { name: 'shop_id' } });
    db.Shop.hasMany(db.Notification, { foreignKey: { name: 'shop_id' } });

    // many - one
    // db.Shop.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
  };

  return Shop;
};

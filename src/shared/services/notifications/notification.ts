export interface INotification {
  via(): string[];
}
export abstract class Notification implements INotification {
  public notifiable: any;
  abstract via(): string[];
}

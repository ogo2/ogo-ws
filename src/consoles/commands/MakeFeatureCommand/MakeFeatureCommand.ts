import { Command, Error, Info } from '../Command';
import fse from 'fs-extra';
import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import { IOption } from '../../interface/IOption';
import { getMockController } from './mocks/controller.mock';
import { getMockService } from './mocks/service.mock';
import { getMockTest } from './mocks/module-test.mock';

const pathDir = '../../../components';

export class MakeFeatureCommand extends Command {
  signature(): string {
    return 'make:feature <feature>';
  }

  description(): string {
    return 'Create new feature fouder';
  }

  options(): IOption[] {
    return [{ key: 'override?', description: 'Override existing fouder' }];
  }

  async handle(feature: string, options: { override: undefined | string }): Promise<any> {
    if (_.isNil(feature) || feature === '') {
      Error('Feature name is required');
    }

    const file = path.resolve(__dirname, pathDir, `${feature}`);
    if (fs.existsSync(file) && (options.override === undefined || options.override.toString() !== 'true')) {
      Error(`${feature} already exist`);
    }
    const arrayName = feature.split('-');
    const upperCaseControllerName =
      arrayName.length > 1
        ? arrayName.reduce((f, b) => f.charAt(0).toUpperCase() + f.slice(1) + b.charAt(0).toUpperCase() + b.slice(1))
        : arrayName[0].charAt(0).toUpperCase() + arrayName[0].slice(1);
    const controllerName = upperCaseControllerName + 'Controller';
    const serviceName = upperCaseControllerName + 'Service';

    const controllerFile = path.resolve(__dirname, pathDir, `${feature}`, 'controllers', `${feature}.controller.ts`);
    const contentController = getMockController(feature, upperCaseControllerName, controllerName, serviceName);
    fse.outputFileSync(controllerFile, contentController);

    const controllerTestFile = path.resolve(
      __dirname,
      pathDir,
      `${feature}`,
      'controllers',
      `${feature}.controller.test.ts`,
    );

    const contentTestController = getMockTest(controllerName, `${feature}.controller`);
    fse.outputFileSync(controllerTestFile, contentTestController);

    const serviceFile = path.resolve(__dirname, pathDir, `${feature}`, 'services', `${feature}.service.ts`);

    const contentService = getMockService(upperCaseControllerName, serviceName);
    fse.outputFileSync(serviceFile, contentService);
    const serviceTestFile = path.resolve(__dirname, pathDir, `${feature}`, 'services', `${feature}.service.test.ts`);
    const contentTestService = getMockTest(serviceName, `${feature}.service`);
    fse.outputFileSync(serviceTestFile, contentTestService);

    Info(`${file} is created`);

    return file;
  }
}

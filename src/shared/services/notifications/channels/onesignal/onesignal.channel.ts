import { singleton } from 'tsyringe';
import { IChannel } from '../../interfaces/channel.interface';
import { ONESIGNAL } from './constants';
import { INotification } from '../../interfaces';
import axios, { AxiosInstance } from 'axios';
import { logger } from 'shared/logger/logger';

export type OptionOneSignal = {
  onesignal_authorization: string;
};

@singleton()
export class OneSignalChannel implements IChannel {
  public name: string;
  private apiInstance: AxiosInstance;

  constructor(options: { name: string; apiInstance: AxiosInstance }) {
    const { name, apiInstance } = options;
    this.name = name;
    this.apiInstance = apiInstance;
  }

  public static config(options: OptionOneSignal) {
    const apiInstance = axios.create({
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        Authorization: `Basic ${options.onesignal_authorization}`,
      },
      timeout: 20000,
      responseType: 'json',
      baseURL: 'https://onesignal.com',
    });

    return new OneSignalChannel({ name: ONESIGNAL, apiInstance });
  }

  async execute(notification: INotification): Promise<any> {
    const onesignalable = (notification as any).toDevice(notification.notifiable);
    // console.log('params', onesignalable.getParams())
    try {
      const result = await this.apiInstance.post('/api/v1/notifications', onesignalable.getParams());
      return result;
    } catch (error) {
      logger.error({ message: error.response.data });
    }
  }
}

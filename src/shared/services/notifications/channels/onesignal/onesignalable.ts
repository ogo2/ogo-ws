import environment from 'configs/environment.constants';
import _ from 'lodash';

export interface IOnesignalParams {
  app_id: string;
  android_channel_id: string;
  data?: any;
  headings: {
    en: string;
  };
  contents: {
    en: string;
  };
  include_player_ids: string[];
}

export interface IOnesignalable {
  toOne(key: string): IOnesignalable;
  toMany(key: string[]): IOnesignalable;
  setData(data: any): IOnesignalable;
  setEnHeading(en_heading: string): IOnesignalable;
  setEnContent(en_content: string): IOnesignalable;
  getParams(): IOnesignalParams;
}

export class Onesingalable implements IOnesignalable {
  private _app_id = environment.onesignal_app_id;
  private _android_channel_id = environment.onesignal_android_channel_id;
  private _data;
  private _headings = {
    en: '',
  };
  private _contents = {
    en: '',
  };
  private _include_player_ids: string[] = [];

  /**
   * push notification to one device
   * @param key_ids
   * @returns
   */
  public toOne(key_ids: string) {
    this._include_player_ids.push(key_ids);
    return this;
  }
  /**
   *  push notification to list device
   * @param key_ids
   * @returns
   */
  public toMany(key_ids: string[]) {
    this._include_player_ids.push(...key_ids);
    return this;
  }

  /**
   * set data params pass to device
   * @param data
   * @returns
   */
  public setData(data: any) {
    this._data = data;
    return this;
  }

  /**
   * set title heading of notification
   * @param en_heading
   * @returns
   */
  public setEnHeading(en_heading: string) {
    this._headings.en = en_heading;
    return this;
  }

  /**
   * set content of notification
   * @param en_content
   * @returns
   */
  public setEnContent(en_content: string) {
    this._contents.en = en_content;
    return this;
  }

  /**
   * get params notification
   * @returns
   */
  public getParams() {
    if (this._include_player_ids.length === 0) throw new Error('Player ids device is required.');
    return {
      app_id: this._app_id,
      data: this._data,
      headings: this._headings,
      contents: this._contents,
      android_channel_id: this._android_channel_id,
      include_player_ids: this._include_player_ids,
    };
  }
}

import { IS_ACTIVE, ORDER_STATUS } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Order = sequelize.define(
    'Order',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      code: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      user_address_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      shop_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      note: {
        allowNull: true,
        type: DataTypes.TEXT,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(ORDER_STATUS),
        defaultValue: ORDER_STATUS.PENDING,
        validate: {
          isIn: {
            args: [Object.values(ORDER_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      address: {
        allowNull: false,
        type: DataTypes.JSON,
      },
      gift_code: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      total_price: { allowNull: true, type: DataTypes.INTEGER },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      pancake_order_id: {
        allowNull: true,
        type: DataTypes.STRING,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'order',
      version: true,
      hooks: {},
    },
  );

  Order.associate = (db) => {
    // one - many
    db.Order.hasMany(db.OrderItem, { foreignKey: { name: 'order_id' } });
    db.Order.hasMany(db.Review, { foreignKey: { name: 'order_id' } });
    db.Order.hasMany(db.OrderStatusHistory, {
      foreignKey: { name: 'order_id' },
    });
    db.Order.hasMany(db.PurchasedGift, {
      foreignKey: { name: 'order_id' },
    });

    // many - one
    db.Order.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Order.belongsTo(db.UserAddress, {
      foreignKey: { name: 'user_address_id' },
    });
    db.Order.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
  };

  Order.generateCodeOrder = function generateCodeOrder(): String {
    const date = Date.now().toString();
    const a: string = date.substring(9);
    const b: string = date.substring(7, 11);
    const min: number = Math.ceil(1000);
    const max: number = Math.floor(9999);
    const c: string = (Math.floor(Math.random() * (max - min)) + min).toString();
    return `#OR${a}${b}${c}`;
  };

  return Order;
};

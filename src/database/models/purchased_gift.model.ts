import { IS_ACTIVE, GIFT_STATUS } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const PurchasedGift = sequelize.define(
    'PurchasedGift',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      gift_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      address: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      order_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(GIFT_STATUS),
        // defaultValue: GIFT_STATUS.PENDING,
        validate: {
          isIn: {
            args: [Object.values(GIFT_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'purchased_gift',
      version: true,
      hooks: {},
    },
  );

  PurchasedGift.associate = (db) => {
    // many - one
    db.PurchasedGift.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.PurchasedGift.belongsTo(db.Gift, { foreignKey: { name: 'gift_id' } });
    // one - many
    db.PurchasedGift.belongsTo(db.Order, {
      foreignKey: { name: 'order_id' },
    });
  };

  return PurchasedGift;
};

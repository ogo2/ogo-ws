import { singleton } from 'tsyringe';

@singleton()
export class WsResponseService {
  public withData<T>(data: T, type_action: number, option?: { message?: string; code?: number }) {
    return {
      status: 1,
      code: option?.code || 1,
      message: option?.message || 'Thành công',
      data,
      type_action,
      create_at: new Date(),
    };
  }
}

import { IS_ACTIVE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const LivestreamLayout = sequelize.define(
    'LivestreamLayout',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      livestream_id: { allowNull: false, type: DataTypes.INTEGER },
      title: { allowNull: true, type: DataTypes.STRING },
      background_color: { allowNull: true, type: DataTypes.STRING },
      text_color: { allowNull: true, type: DataTypes.STRING },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'livestream_layout',
      version: true,
      hooks: {},
    },
  );

  LivestreamLayout.associate = (db) => {
    // many - one
    db.LivestreamLayout.belongsTo(db.Livestream, { foreignKey: { name: 'livestream_id' } });
  };
  return LivestreamLayout;
};

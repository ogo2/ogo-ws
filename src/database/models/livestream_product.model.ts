import { IS_ACTIVE, IS_HIGHLIGHT_LIVESTREAM_PRODUCT } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const LivestreamProduct = sequelize.define(
    'LivestreamProduct',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      livestream_id: { allowNull: false, type: DataTypes.INTEGER },
      product_id: { allowNull: false, type: DataTypes.INTEGER },
      code_product_livestream: { allowNull: true, type: DataTypes.STRING },
      product: { allowNull: false, type: DataTypes.JSON },
      is_highlight: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_HIGHLIGHT_LIVESTREAM_PRODUCT),
        defaultValue: IS_HIGHLIGHT_LIVESTREAM_PRODUCT.NOT_HIGHLIGHT,
        validate: {
          isIn: {
            args: [Object.values(IS_HIGHLIGHT_LIVESTREAM_PRODUCT)],
            msg: 'Invalid is highlight product.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'livestream_product',
      version: true,
      hooks: {},
    },
  );

  LivestreamProduct.associate = (db) => {
    // many - one
    db.LivestreamProduct.belongsTo(db.Product, { foreignKey: { name: 'product_id' } });
    db.LivestreamProduct.belongsTo(db.Livestream, { foreignKey: { name: 'livestream_id' } });
  };
  return LivestreamProduct;
};

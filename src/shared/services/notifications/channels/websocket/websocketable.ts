import _ from 'lodash';

export interface IWebSocketParams {
  event: string;
  to: string[];
  data: any;
}

export interface IWebSocketable {
  toEvent(event: string): IWebSocketable;
  to(to: string): IWebSocketable;
  toMany(to: string[]): IWebSocketable;
  setData(data?: any): IWebSocketable;
  getParams(): IWebSocketParams;
}

export class WebSocketable implements IWebSocketable {
  private _event: string;
  private _to: string[] = [];
  private _data: any;

  /**
   * set event to send
   * @param event
   * @returns
   */
  public toEvent(event: string) {
    this._event = event;
    return this;
  }

  /**
   * set event to a room channel or a socket id
   * @param event
   * @returns
   */
  public to(to: string) {
    this._to.push(to);
    return this;
  }

  /**
   * set event to rooms channel or socket ids
   * @param event
   * @returns
   */
  public toMany(to: string[]) {
    this._to.push(...to);
    return this;
  }

  /**
   * set data to emit
   * @param event
   * @returns
   */
  public setData(data: any) {
    this._data = data;
    return this;
  }

  /**
   * get params socket notification
   * @returns
   */
  public getParams() {
    return {
      event: this._event,
      to: this._to,
      data: this._data,
    };
  }
}

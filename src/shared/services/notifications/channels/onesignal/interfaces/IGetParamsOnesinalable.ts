import { IOnesignalable } from '../onesignalable';

export interface IGetParamsOnesinalable {
  toDevice(): IOnesignalable | Promise<IOnesignalable>;
}

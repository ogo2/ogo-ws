import { MakeSeederCommand } from './commands/MakeSeederCommand/MakeSeederCommand';
import { MakeModelCommand } from './commands/MakeModelCommand/MakeModelCommand';
import { MakeFeatureCommand } from './commands/MakeFeatureCommand/MakeFeatureCommand';
export class Kernel {
  public static instance: Kernel;

  public static getInstance() {
    if (!Kernel.instance) {
      Kernel.instance = new Kernel();
    }
    return Kernel.instance;
  }
  commands() {
    return {
      'make:feature': MakeFeatureCommand,
      'make:seeder': MakeSeederCommand,
      'make:model': MakeModelCommand,
    };
  }
}

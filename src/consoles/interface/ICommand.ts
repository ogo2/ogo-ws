import { IOption } from './IOption';

export interface ICommand {
  signature(): string;
  description(): string;
  options(): IOption[];
}

#!/bin/bash
TAG=dev
if [[ $1 = 'ogo_ws' ]]; then
        docker pull windsoft2018/ogo_ws:${TAG}
        docker-compose stop ogo_ws
        docker-compose up -d ogo_ws
        exit 0
fi


docker image prune -a -f
export const getMockTest = (upperCaseModule: string, file: string) => `import * as faker from 'faker';
import 'reflect-metadata';
import { container } from 'tsyringe';
import { ${upperCaseModule} } from './${file}';

describe('${upperCaseModule}', () => {
  let service: ${upperCaseModule};
  beforeEach(async () => {
    container.clearInstances();
    service = container.resolve(${upperCaseModule});
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
`;

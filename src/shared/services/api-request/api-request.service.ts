import { PAGING_CONFIG } from 'configs/configs.constants';
import environment from 'configs/environment.constants';
import { singleton } from 'tsyringe';

export type PagingConfig = {
  page: number;
  limit: number;
  offset: number;
};
@singleton()
export class ApiRequestService {
  public handlePagingParams(page?: number, limit?: number, offset?: number): PagingConfig {
    page = page || 1;
    limit = limit || PAGING_CONFIG.LIMIT;
    if (limit <= 0 || limit > PAGING_CONFIG.MAX) {
      limit = PAGING_CONFIG.LIMIT;
    }
    page = Math.max(page, 1);
    offset = (page - 1) * limit;
    return { page, limit, offset };
  }
  public performFromDateToDate(
    from_date: Date | number | string,
    to_date: Date | number | string,
  ): { from_date: Date | number; to_date: Date | number } {
    if (from_date) {
      from_date = new Date(from_date);
    } else from_date = 0;
    if (to_date) {
      to_date = new Date(new Date(to_date).setDate(new Date(to_date).getDate() + 1));
    } else to_date = new Date(Date.now());
    return {
      from_date,
      to_date,
    };
  }
}

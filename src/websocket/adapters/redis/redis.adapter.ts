import { RedisClient } from 'redis';
import { getRefRedisConnection } from './redis.provider';
import { IIOAdapter } from 'websocket/interfaces/IAdapter';
import { createAdapter } from '@socket.io/redis-adapter';

export class RedisAdapter implements IIOAdapter {
  private pubClient: RedisClient;
  private subClient: RedisClient;
  constructor() {
    this.pubClient = getRefRedisConnection();
    this.subClient = getRefRedisConnection();
  }
  createAdapter() {
    const adapter: any = createAdapter(this.pubClient, this.subClient);
    return adapter;
  }
}

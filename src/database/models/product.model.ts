import { IS_ACTIVE, PRODUCT_STATUS, PRODUCT_STOCK_STATUS } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Product = sequelize.define(
    'Product',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      shop_id: { allowNull: false, type: DataTypes.INTEGER },
      category_id: { allowNull: false, type: DataTypes.INTEGER },
      code: { allowNull: false, type: DataTypes.STRING },
      name: { allowNull: false, type: DataTypes.STRING },
      description: { allowNull: true, type: DataTypes.TEXT },
      price: { type: DataTypes.INTEGER },
      star: { allowNull: true, type: DataTypes.FLOAT },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(PRODUCT_STATUS),
        defaultValue: PRODUCT_STATUS.AVAILABLE,
        validate: {
          isIn: {
            args: [Object.values(PRODUCT_STATUS)],
            msg: 'Invalid status.',
          },
        },
      },
      stock_status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(PRODUCT_STOCK_STATUS),
        defaultValue: PRODUCT_STOCK_STATUS.AVAILABLE,
        validate: {
          isIn: {
            args: [Object.values(PRODUCT_STOCK_STATUS)],
            msg: 'Invalid stock status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      product_pancake_id: {
        allowNull: true,
        type: DataTypes.STRING,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'product',
      version: true,
      hooks: {},
    },
  );

  Product.associate = (db) => {
    // one - many
    db.Product.hasMany(db.ProductMedia, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.ProductStock, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.LivestreamProduct, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.OrderItem, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.ProductPrice, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.ProductCustomAttribute, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.Review, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.Wishlist, { foreignKey: { name: 'product_id' } });
    db.Product.hasMany(db.Post, { foreignKey: { name: 'product_id' } });
    // many - one
    db.Product.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.Product.belongsTo(db.Category, { foreignKey: { name: 'category_id' } });
  };

  return Product;
};

export const API_DOCS = '/docs/v1';
export const API_VERSION = '/api/v1';
export const PAGING_CONFIG = {
  LIMIT: 12,
  MAX: 1000,
};

export const OTP_CONFIG_VERIFY_PHONE = {
  LIMIT: 5,
  MINUS_EXPIRED: 2, // 2 minus
  HOUR_CAPABLE_OF_SENDING: 3, // 3 hour
};
export const OTP_CONFIG_FORGOT_PASSWORD_MAIL = {
  LIMIT: 5,
  MINUS_EXPIRED: 2, // 2 minus
  HOUR_CAPABLE_OF_SENDING: 3, // 3 hour
};

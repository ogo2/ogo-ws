export enum SuccessMessage {
  Geted = 'Get resources successfully',
  Created = 'Create resource success.',
  Updated = 'Update resource success.',
  Deleted = 'Delete resource success.',
}

export enum ErrorMessage {
  Validate = 'Lỗi thẩm định dữ liệu',
  Conflict = 'Lỗi xung đột',
  NotFound = 'Không tìm thấy tài nguyên',
  InValid = 'Lỗi không hợp lệ',
  Forbidden = 'Tài khoản bị cấm sử dụng chức năng này',
  UnAuthorized = 'Phiên đăng nhập hết hạn.',
  Server = 'Lỗi kết nối máy chủ',
  HmacInvalid = 'Hmac Signature Invalid',
}
export enum HttpCode {
  Success = 200,
  Created = 201,
  NotFound = 404,
  BadRequest = 400,
  Server = 500,
  UnAuthorized = 401,
  Conflict = 409,
  Forbidden = 403,
}

import { singleton } from 'tsyringe';
import { IChannel } from '../../interfaces/channel.interface';
import { MAIL } from './constants';
import { INotification } from '../../interfaces';
import * as nodemailer from 'nodemailer';
import { logger } from 'shared/logger/logger';

export type OptionEMailChannel = {
  host: string;
  port: number;
  secure: boolean;
  auth: {
    user: string;
    pass: string;
  };
};
@singleton()
export class EmailChannel implements IChannel {
  public name: string;
  public transporter: nodemailer.Transporter;

  constructor(options: { name: string; transporter: nodemailer.Transporter }) {
    const { name, transporter } = options;
    this.name = name;
    this.transporter = transporter;
  }

  public static config(options: OptionEMailChannel) {
    const transporter = nodemailer.createTransport(options);
    return new EmailChannel({ name: MAIL, transporter });
  }

  async execute(notification: INotification): Promise<any> {
    const mailable = (notification as any).toMail(notification.notifiable);
    try {
      const result = await this.transporter.sendMail(mailable.getParams());
      return result;
    } catch (error) {
      logger.error({ message: error });
    }
  }
}

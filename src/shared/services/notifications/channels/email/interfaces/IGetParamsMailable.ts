import { IMailable } from '../mailable';

export interface IGetParamsMailable {
  toMail(): IMailable | Promise<IMailable>;
}

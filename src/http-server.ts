import { Server } from 'http';
import { Express } from 'express';

export class HttpServer extends Server {
  constructor(app: Express) {
    super(app);
  }
  public static createServer(app: Express) {
    return new HttpServer(app);
  }
}

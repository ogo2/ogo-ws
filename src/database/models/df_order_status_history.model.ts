import { IS_ACTIVE } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const DFOrderStatusHistory = sequelize.define(
    'DFOrderStatusHistory',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      order: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      describe: {
        type: DataTypes.STRING,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'df_order_status_history',
      version: true,
      hooks: {},
    },
  );

  DFOrderStatusHistory.associate = (db) => {
    // one - many
    db.DFOrderStatusHistory.hasMany(db.OrderStatusHistory, {
      foreignKey: { name: 'df_order_status_history_id' },
    });
  };

  return DFOrderStatusHistory;
};

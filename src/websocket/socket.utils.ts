import { ConflictErrorResponse, UnAuthorizedErrorResponse } from 'shared/services/api-response/models/errors';
import { singleton, inject } from 'tsyringe';
import { SOCKET_SERVER } from '../app.constants';
import { Server as SocketServer } from 'socket.io';
const db = require('database/models');
const { Sequelize, sequelize, User } = db.default;
const { Op } = Sequelize;

@singleton()
export class SocketUtils {
  constructor(@inject(SOCKET_SERVER) private readonly socketServer: SocketServer) {}
  public countUserInSocketRoom(channel: string) {
    const hasRoomExisted = this.socketServer.sockets.adapter.rooms.has(channel);
    if (!hasRoomExisted) return null;
    // reason -1: subtract user publish livestream
    return this.socketServer.sockets.adapter.rooms.get(channel).size - 1;
  }
  async countUserInAdapterSocketRoom(channel: string) {
    const sSockets = await this.socketServer.in(channel).allSockets();
    if (sSockets.size === 0) return null;
    // reason -1: subtract user publish livestream
    return sSockets.size - 1;
  }
  public getAllRoom() {
    return this.socketServer.sockets.adapter.rooms;
  }
}

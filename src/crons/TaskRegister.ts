import { ICronJob } from './interfaces/ICronJob';
export class TaskRegister {
  private static readonly _crons: ICronJob[] = [];
  /**
   * register cron job
   * @param cron ICronJob
   * @returns none
   */
  public static register = (cron: ICronJob) => {
    TaskRegister._crons.push(cron);
    return TaskRegister;
  };

  /**
   * enforce all cron job registed
   * @param cron ICronJob
   * @returns none
   */
  public static enforce = () => {
    TaskRegister._crons.map((cron: ICronJob) => cron.execute());
    return TaskRegister;
  };
}

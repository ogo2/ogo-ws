import { ISmsable } from '../smsable';

export interface IGetParamsSmsable {
  toPhone(): ISmsable | Promise<ISmsable>;
}

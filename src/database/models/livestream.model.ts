import { LIVESTREAM_STATUS, IS_ACTIVE, getFullUrl } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Livestream = sequelize.define(
    'Livestream',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: { allowNull: false, type: DataTypes.INTEGER },
      shop_id: { allowNull: false, type: DataTypes.INTEGER },
      title: { allowNull: true, type: DataTypes.STRING },
      cover_image_url: {
        allowNull: true,
        type: DataTypes.STRING,
        get() {
          return getFullUrl(this.getDataValue('cover_image_url'));
        },
      },

      // ignore agora servive
      token_publisher: {
        // unique: true,
        allowNull: true,
        type: DataTypes.STRING,
      },
      record_uid: { allowNull: true, type: DataTypes.INTEGER },
      record_sid: { allowNull: true, type: DataTypes.STRING },
      record_resourceid: {
        allowNull: true,
        type: DataTypes.TEXT,
      },
      minutes_used_before_livestream: {
        allowNull: true,
        type: DataTypes.BIGINT,
      },
      //

      // google streaming
      broadcast_id: { allowNull: true, type: DataTypes.STRING },
      stream_id: { allowNull: true, type: DataTypes.STRING },
      cdn: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      //

      livestream_record_url: { allowNull: true, type: DataTypes.STRING },
      count_viewed: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      count_reaction: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      count_comment: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      start_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      finish_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      expire_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      ping_at: {
        allowNull: true,
        type: DataTypes.DATE,
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(LIVESTREAM_STATUS),
        defaultValue: LIVESTREAM_STATUS.INITIAL,
        validate: {
          isIn: {
            args: [Object.values(LIVESTREAM_STATUS)],
            msg: 'Invalid live stream status.',
          },
        },
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'livestream',
      version: true,
      hooks: {},
    },
  );

  Livestream.associate = (db) => {
    // one - many
    db.Livestream.hasMany(db.UserWatched, {
      foreignKey: { name: 'livestream_id' },
    });
    db.Livestream.hasMany(db.Reaction, {
      foreignKey: { name: 'livestream_id' },
    });
    db.Livestream.hasMany(db.Comment, {
      foreignKey: { name: 'livestream_id' },
    });
    db.Livestream.hasMany(db.LivestreamLayout, {
      foreignKey: { name: 'livestream_id' },
    });
    db.Livestream.hasMany(db.LivestreamProduct, {
      foreignKey: { name: 'livestream_id' },
    });

    // many - one
    db.Livestream.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Livestream.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
  };
  return Livestream;
};

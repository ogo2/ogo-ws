import { Pagination } from './Pagination';

export type PaginateResponse<T> = {
  data: T[];
  meta: { pagination: Pagination };
  code?: number;
  status?: boolean;
  message?: string;
};

import { DependencyContainer, container } from 'tsyringe';
import { SharedModule } from 'shared/shared.module';
import { ComponentModule } from 'components/components.module';
import { HTTP_SERVER, SOCKET_SERVER } from './app.constants';
import { HttpServer } from './http-server';
import { createApp } from './app';
import { WebSocket } from './websocket/ws';

export interface IModule {
  registers: DependencyContainer[];
  imports: IModule[] | [];
}

export class AppModule implements IModule {
  registers = [
    container.register(HTTP_SERVER, {
      useValue: HttpServer.createServer(createApp()),
    }),
    container.register(SOCKET_SERVER, {
      useValue: WebSocket.createWebSocket(container.resolve(HTTP_SERVER)),
    }),
  ];
  imports = [new ComponentModule(), new SharedModule()];
}

export default new AppModule();

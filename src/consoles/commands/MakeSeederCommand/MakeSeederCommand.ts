import { Command, Error, Info } from '../Command';
import fse from 'fs-extra';
import fs from 'fs';
import path from 'path';
import moment from 'moment';
import _ from 'lodash';
import { IOption } from '../../interface/IOption';
import { getMockSeeder } from './mocks/seeder.mock';
const pathDir = '../../../database/seeders';

export class MakeSeederCommand extends Command {
  signature(): string {
    return 'make:seeder <seeder>';
  }

  description(): string {
    return 'Create new seeder file';
  }

  options(): IOption[] {
    return [{ key: 'override?', description: 'Override existing file' }];
  }

  async handle(seeder: string, options: { override: undefined | string }): Promise<any> {
    if (_.isNil(seeder) || seeder === '') {
      Error('Seeder name is required');
    }
    const file = path.resolve(__dirname, pathDir, `${moment().format('YYYYMMDD_HHmmss')}_${seeder}.ts`);
    if (fs.existsSync(file) && (options.override === undefined || options.override.toString() !== 'true')) {
      Error(`${seeder} already exist`);
    }
    const contentSeeder = getMockSeeder();
    fse.outputFileSync(file, contentSeeder);
    Info(`${file} is created`);

    return file;
  }
}

import { IS_ACTIVE, IS_READ } from 'database/database.constants';

module.exports = function (sequelize, DataTypes) {
  const Notification = sequelize.define(
    'Notification',
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      content: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      is_read: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: IS_READ.NOT_READ,
        values: Object.values(IS_READ),
        validate: {
          isIn: {
            args: [Object.values(IS_READ)],
            msg: 'Invalid is read message.',
          },
        },
      },
      url: {
        allowNull: true,
        type: DataTypes.STRING,
      },
      data: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      df_notification_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
      is_push: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: 0,
      },
      is_active: {
        allowNull: false,
        type: DataTypes.INTEGER,
        values: Object.values(IS_ACTIVE),
        defaultValue: IS_ACTIVE.ACTIVE,
        validate: {
          isIn: {
            args: [Object.values(IS_ACTIVE)],
            msg: 'Invalid status.',
          },
        },
      },
      create_by: DataTypes.INTEGER,
      update_by: DataTypes.INTEGER,
      delete_by: DataTypes.INTEGER,
      create_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      update_at: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      },
      delete_at: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: false,
      timestamps: false,
      freezeTableName: true,
      tableName: 'notification',
      version: true,
      hooks: {},
    },
  );

  Notification.associate = (db) => {
    // many - one
    db.Notification.belongsTo(db.User, { foreignKey: { name: 'user_id' } });
    db.Notification.belongsTo(db.Shop, { foreignKey: { name: 'shop_id' } });
    db.Notification.belongsTo(db.DFNotification, {
      foreignKey: { name: 'df_notification_id' },
    });
  };

  return Notification;
};

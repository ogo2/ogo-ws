import { IModule } from '../app.module';
import { NotificationModule } from './services/notifications/notification.module';

export class SharedModule implements IModule {
  registers = [];
  imports = [new NotificationModule()];
}

export interface JWTPayload {
  id: number;
  phone: string;
  shop_id?: number | null;
  df_type_user_id: number;
}

import { IWebSocketable } from '../websocketable';

export interface IGetParamsWebsocketable {
  toChannel(): IWebSocketable | Promise<IWebSocketable>;
}

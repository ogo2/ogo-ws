import { IModule } from '../app.module';

export class ComponentModule implements IModule {
  registers = [];
  imports = [];
}
